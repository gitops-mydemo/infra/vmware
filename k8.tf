module "k8" {
  source           = "/k8"
  cluster_name     = "gitops-demo-local"
  cluster_version  = "1.17"
  subnets          = module.network.public_subnets
  write_kubeconfig = "false"
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
  vlan_id = module.network.vlan_id

  worker_groups = [
    {
      instance_size = "large"
      asg_max_size  = 5
      tags = [{
        key                 = "Terraform"
        value               = "true"
        propagate_at_launch = true
      }]
    }
  ]
}

output "env-dynamic-url" {
  value = module.k8.cluster_endpoint
}
